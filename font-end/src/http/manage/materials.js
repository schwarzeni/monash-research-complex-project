import {materialApi} from '../apiList'

export function generateMaterialPostModel (name, description) {
  return {
    name: name,
    description: description
  }
}

export function getMaterials (offset, limit, $http) {
  return new Promise((resolve, reject) => {
    $http.get(materialApi, {params: {
      offset: offset,
      limit: limit
    }})
      .then(response => {
        resolve(response.body.materials)
      }, response => {
        // TODO: handle err here
        console.log(response.body.error)
        reject(response.body.error)
      })
  })
}

export function addNewMaterials (material, $http) {
  if (!material || !material.name || !material.description) {
    throw new Error('物资数据不完整')
  }
  return new Promise((resolve, reject) => {
    $http.post(materialApi, {material: {
      name: material.name,
      description: material.description
    }})
      .then(response => {
        resolve(response.body.material)
      }, response => {
        // TODO: handle error here
        console.log(response)
        reject(response)
      })
  })
}
