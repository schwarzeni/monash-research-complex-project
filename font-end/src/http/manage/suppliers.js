import {supplierApi} from '../apiList'

export function generateSupplierPostModel (name, tel, address) {
  return {
    name: name,
    tel: tel,
    address: address
  }
}

export function getSuppliers (offset, limit, $http) {
  return new Promise((resolve, reject) => {
    $http.get(supplierApi, {params: {
      offset: offset,
      limit: limit
    }})
      .then(response => {
        resolve(response.body.suppliers)
      }, response => {
        // TODO: handle err here
        console.log(response.body.error)
        reject(response.body.error)
      })
  })
}

export function addNewSuppliers (supplier, $http) {
  if (!supplier || !supplier.name || !supplier.tel || !supplier.address) {
    throw new Error('供应商数据不完整')
  }
  return new Promise((resolve, reject) => {
    $http.post(supplierApi, {supplier: {
      name: supplier.name,
      tel: supplier.tel,
      address: supplier.address
    }})
      .then(response => {
        // TODO: handle here
        resolve(response.body.supplier)
      }, response => {
        // TODO: handle error here
        console.log(response)
        reject(response)
      })
  })
}
