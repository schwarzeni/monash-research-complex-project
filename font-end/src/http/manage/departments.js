import {departmentApi} from '../apiList'

export function generateDepartmentPostModel (name) {
  return {
    name: name
  }
}

export function getDepartments (offset, limit, $http) {
  return new Promise((resolve, reject) => {
    $http.get(departmentApi, {params: {
      offset: offset,
      limit: limit
    }})
      .then(response => {
        resolve(response.body.departments)
      }, response => {
        // TODO: handle err here
        console.log(response.body.error)
        reject(response.body.error)
      })
  })
}

export function addNewDepartment (department, $http) {
  if (!department || !department.name) {
    throw new Error('部门数据不完整')
  }
  return new Promise((resolve, reject) => {
    $http.post(departmentApi, { department: {
      name: department.name
    }})
      .then(response => {
        resolve(response.body.department)
      }, response => {
        // TODO: handle error here
        console.log(response)
        reject(response)
      })
  })
}
