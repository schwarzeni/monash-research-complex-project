import {storehouseApi} from '../apiList'

export function generateStoreHousePostModel (name, location, size) {
  return {
    name: name,
    location: location,
    size: size
  }
}

export function getStorehouses (offset, limit, $http) {
  return new Promise((resolve, reject) => {
    $http.get(storehouseApi, {params: {
      offset: offset,
      limit: limit
    }})
      .then(response => {
        resolve(response.body.storehouses)
      }, response => {
        // TODO: handle err here
        console.log(response.body.error)
        reject(response.body.error)
      })
  })
}

export function addNewStorehouses (storehouse, $http) {
  if (!storehouse || !storehouse.name || !storehouse.location || !storehouse.size) {
    throw new Error('存储仓库数据不完整')
  }
  return new Promise((resolve, reject) => {
    $http.post(storehouseApi, {storehouse: {
      name: storehouse.name,
      location: storehouse.location,
      size: storehouse.size
    }})
      .then(response => {
        resolve(response.body.storehouse)
      }, response => {
        // TODO: handle error here
        console.log(response)
        reject(response)
      })
  })
}
