import {loginApi, logoutApi} from './apiList'

export function login (email, password, $http) {
  return new Promise((resolve, reject) => {
    $http.post(loginApi, {email: email, password: password})
      .then(response => {
        resolve(response)
      }, response => {
        reject(response)
      })
  })
}

export function logout ($http) {
  return new Promise((resolve, reject) => {
    $http.post(logoutApi)
      .then(response => {
        resolve()
      }, response => {
        reject(response)
      })
  })
}
