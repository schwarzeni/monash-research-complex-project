export const apiServerHost = 'http://localhost:3000/api'
export const apiVersion = 'v1'
export const tokenConfirmApi = `${apiServerHost}/${apiVersion}/auth/tokenConfirm`
export const loginApi = `${apiServerHost}/${apiVersion}/auth/login`
export const logoutApi = `${apiServerHost}/${apiVersion}/auth/logout`

export const supplierApi = `${apiServerHost}/${apiVersion}/manage/suppliers`
export const materialApi = `${apiServerHost}/${apiVersion}/manage/materials`
export const departmentApi = `${apiServerHost}/${apiVersion}/manage/departments`
export const storehouseApi = `${apiServerHost}/${apiVersion}/manage/storehouses`
