// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import VeeValidate, {Validator} from 'vee-validate'
import zh from 'vee-validate/dist/locale/zh_CN'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'

import('./icon.css')
import('../node_modules/vuetify/dist/vuetify.min.css')
Vue.use(Vuetify)
Vue.use(VueResource)
Vue.http.options.credentials = true

Validator.localize('zh_CN', zh)
Vue.use(VeeValidate, {
  locale: 'zh_CN'
})
// index.js or main.js

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
