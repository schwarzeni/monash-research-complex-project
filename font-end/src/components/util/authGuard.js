import {tokenConfirmApi} from '../../http/apiList'
import {loginUrl} from '../../router/routerName'
import {SET_USER_INFO} from '../../store/types/mution-types'

export function isLogin ($http, $route, $store) {
  $http.get(tokenConfirmApi)
    .then(response => {
      setTimeout(() => {
        if ($store) {
          $store.commit(SET_USER_INFO, {
            user: response.body
          })
        }
      }, 1000)
    }, response => {
      $route.push(loginUrl)
    })
}
