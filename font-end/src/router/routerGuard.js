export function dashboardComponentRouterGuard (to, from, next) {
  const regex = /(\/app\/dashboard)/
  if (regex.exec(to.fullPath) !== null) {
    next()
  }
}
