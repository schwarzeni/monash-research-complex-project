import Vue from 'vue'
import Router from 'vue-router'
import {
  adminRecordUrl,
  adminUserUrl, departmentRecordUrl,
  materialsInfoUrl,
  outboundFormUrl,
  stockFlowAccountUrl,
  storeFormUrl,
  storeGeneralLedgerUrl,
  userInfoUrl,
  loginUrl,
  dashboardUrl, suppliersUrl, storesUrl, homeUrl
} from './routerName'

const Default = () => import('../components/dashboard/components/Default.vue')
const UserInfo = () => import('../components/dashboard/components/UserInfo.vue')
const MaterialsInfo = () => import('../components/dashboard/components/MaterialsInfo.vue')
const OutboundForm = () => import('../components/dashboard/components/OutboundForm.vue')
const StockFlowAccount = () => import('../components/dashboard/components/StockFlowAccount.vue')
const StoreForm = () => import('../components/dashboard/components/StoreForm.vue')
const StoreGeneralLedger = () => import('../components/dashboard/components/StoreGeneralLedger.vue')
const DepartmentRecord = () => import('../components/dashboard/components/DepartmentRecord.vue')
const Suppliers = () => import('../components/dashboard/components/Suppliers.vue')
const Stores = () => import('../components/dashboard/components/Stores.vue')
const AdminRecord = () => import('../components/dashboard/components/AdminRecord.vue')
const AdminUser = () => import('../components/dashboard/components/AdminUser.vue')
const Login = () => import('../components/dashboard/components/Login.vue')
// const Dashboard = () => import('../components/dashboard/Dashboard.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: userInfoUrl,
      component: UserInfo
    },
    {
      path: materialsInfoUrl,
      component: MaterialsInfo
    },
    {
      path: outboundFormUrl,
      component: OutboundForm
    },
    {
      path: stockFlowAccountUrl,
      component: StockFlowAccount
    },
    {
      path: storeFormUrl,
      component: StoreForm
    },
    {
      path: storeGeneralLedgerUrl,
      component: StoreGeneralLedger
    },
    {
      path: departmentRecordUrl,
      component: DepartmentRecord
    },
    {
      path: suppliersUrl,
      component: Suppliers
    },
    {
      path: storesUrl,
      component: Stores
    },
    {
      path: adminUserUrl,
      component: AdminUser
    },
    {
      path: adminRecordUrl,
      component: AdminRecord
    },
    {
      path: loginUrl,
      component: Login
    },
    {
      path: dashboardUrl,
      component: Default
    },
    {
      path: homeUrl,
      component: Default
    }
  ]
})
