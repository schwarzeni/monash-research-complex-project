/**
 * @constant
 * @type {string}
 * @default /app/dashboard
 * 页面主操作区
 */
export const dashboardUrl = '/app/dashboard'

/**
 * @constant
 * @type {string}
 * @default /app/dashboard
 * 欢迎界面
 */
export const homeUrl = '/app/dashboard/home'

/**
 * @constant
 * @type {string}
 * @default /app/dashboard/user
 * 对用户进行一系列修改
 */
export const userInfoUrl = '/app/dashboard/user'

/**
 * @constant
 * @type {string}
 * @default /app/info/materialsInfoUrl
 * 物资信息
 */
export const materialsInfoUrl = '/app/info/materialsInfoUrl'

/**
 * @constant
 * @type {string}
 * @default /app/info/outboundOrder
 * 对出库单进行一系列修改
 */
export const outboundFormUrl = '/app/info/outboundOrder'

/**
 * @constant
 * @type {string}
 * @default /app/info/stockFlowAccounts
 * 库存流水
 */
export const stockFlowAccountUrl = '/app/info/stockFlowAccounts'

/**
 * @constant
 * @type {string}
 * @default /app/info/storageList
 * 对入库单进行一系列修改
 */
export const storeFormUrl = '/app/info/storageList'

/**
 * @constant
 * @type {string}
 * @default /app/info/storeGeneralLedger
 * 库存总信息
 */
export const storeGeneralLedgerUrl = '/app/info/storeGeneralLedger'

/**
 * @constant
 * @type {string}
 * @default /app/info/departmentRecord
 * 部门调用记录
 */
export const departmentRecordUrl = '/app/info/departmentRecord'

/**
 * @constant
 * @type {string}
 * @default /app/info/suppliers
 * 供应商登记
 */
export const suppliersUrl = '/app/info/suppliers'

/**
 * @constant
 * @type {string}
 * @default /app/info/stores
 * 仓库登记
 */
export const storesUrl = '/app/info/stores'

/**
 * @constant
 * @type {string}
 * @default /app/dashboard/admin/user
 * 管理用户
 */
export const adminUserUrl = '/app/dashboard/admin/user'

/**
 * @constant
 * @type {string}
 * @default /app/dashboard/admin/record
 * 查看用户行为记录记录
 */
export const adminRecordUrl = '/app/dashboard/admin/record'

/**
 * @constant
 * @type {string}
 * @default /app/login
 */
export const loginUrl = '/app/login'
