import {INIT_MATERIALS_INFO, ADD_NEW_MATERIALS_INFO} from '../types/mution-types'

export const materials = {
  state: {
    materialsInfo: []
  },
  getters: {
    getMaterialsInfo (state) {
      return state.materialsInfo
    }
  },
  mutations: {
    [INIT_MATERIALS_INFO] (state, payload) {
      if (!payload.materials) {
        throw new Error('empty materials')
      }
      state.materialsInfo = payload.materials
    },
    [ADD_NEW_MATERIALS_INFO] (state, payload) {
      if (!payload.material) {
        throw new Error('empty material')
      }
      state.materialsInfo.push(payload.material)
    }
  }
}
