import {INIT_DEPARTMENTS_INFO, ADD_NEW_DEPARTMENTS_INFO} from '../types/mution-types'

export const departments = {
  state: {
    departmentsInfo: []
  },
  getters: {
    getDepartmentsInfo (state) {
      return state.departmentsInfo
    }
  },
  mutations: {
    [INIT_DEPARTMENTS_INFO] (state, payload) {
      if (!payload.departments) {
        throw new Error('empty departments')
      }
      state.departmentsInfo = payload.departments
    },
    [ADD_NEW_DEPARTMENTS_INFO] (state, payload) {
      if (!payload.department) {
        throw new Error('empty department')
      }
      state.departmentsInfo.push(payload.department)
    }
  }
}
