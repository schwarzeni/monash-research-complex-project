import {SET_USER_INFO} from '../types/mution-types'
export const user = {
  state: {
    info: {
      id: -1,
      name: '',
      email: '',
      authLevel: ''
    }
  },
  getters: {
    getUserInfo (state) {
      return state.info
    }
  },
  mutations: {
    [SET_USER_INFO] (state, payload) {
      if (!payload.user) {
        throw new Error('Empty user')
      }
      state.info = {...state.info, ...payload.user}
    }
  }
}
