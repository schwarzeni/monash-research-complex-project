import {ADD_NEW_STOREHOUSES_INFO, INIT_STOREHOUSES_INFO} from '../types/mution-types'
export const storehouses = {
  state: {
    storehousesInfo: []
  },
  getters: {
    getStoreHousesInfo (state) {
      return state.storehousesInfo
    }
  },
  mutations: {
    [INIT_STOREHOUSES_INFO] (state, payload) {
      if (!payload.storehouses) {
        throw new Error('empty storehouses')
      }
      state.storehousesInfo = payload.storehouses
    },
    [ADD_NEW_STOREHOUSES_INFO] (state, payload) {
      if (!payload.storehouse) {
        throw new Error('empty storehouse')
      }
      state.storehousesInfo.push(payload.storehouse)
    }
  }
}
