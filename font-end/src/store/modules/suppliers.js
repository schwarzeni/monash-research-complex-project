import {INIT_SUPPLIERS_INFO, ADD_NEW_SUPPLIERS_INFO} from '../types/mution-types'

/**
 *  supplier types: {supplier_id: ,address: ,tel: ,name}
 * @type {{state: {suppliersInfo: Array}}}
 */
export const suppliers = {
  state: {
    suppliersInfo: []
  },
  getters: {
    getSuppliersInfo (state) {
      return state.suppliersInfo
    }
  },
  mutations: {
    [INIT_SUPPLIERS_INFO] (state, payload) {
      if (!payload.suppliers) {
        throw new Error('empty suppliers')
      }
      state.suppliersInfo = payload.suppliers
    },
    [ADD_NEW_SUPPLIERS_INFO] (state, payload) {
      if (!payload.supplier) {
        throw new Error('empty supplier')
      }
      state.suppliersInfo.push(payload.supplier)
    }
  }
}
