export const SET_USER_INFO = 'SET_USER_INFO'

/**
 * 初始化供应商数据
 * @type {string}
 */
export const INIT_SUPPLIERS_INFO = 'INIT_SUPPLIERS_INFO'

/**
 * 新增一条供应商数据
 * @type {string}
 */
export const ADD_NEW_SUPPLIERS_INFO = 'ADD_NEW_SUPPLIERS_INFO'

/**
 * 初始化物资信息
 * @type {string}
 */
export const INIT_MATERIALS_INFO = 'INIT_MATERIALS_INFO'

/**
 * 添加一条新的物资的信息
 * @type {string}
 */
export const ADD_NEW_MATERIALS_INFO = 'ADD_NEW_MATERIALS_INFO'

/**
 * 初始化部门信息
 * @type {string}
 */
export const INIT_DEPARTMENTS_INFO = 'INIT_DEPARTMENTS_INFO'

/**
 * 添加一条新的部门信息
 * @type {string}
 */
export const ADD_NEW_DEPARTMENTS_INFO = 'ADD_NEW_DEPARTMENTS_INFO'

/**
 * 初始化库房信息
 * @type {string}
 */
export const INIT_STOREHOUSES_INFO = 'INIT_STOREHOUSES_INFO'

/**
 * 添加一条新的库房信息
 * @type {string}
 */
export const ADD_NEW_STOREHOUSES_INFO = 'ADD_NEW_STOREHOUSES_INFO'
