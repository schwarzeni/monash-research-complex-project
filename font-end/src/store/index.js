import Vuex from 'vuex'
import Vue from 'vue'
import {user} from './modules/user'
import {suppliers} from './modules/suppliers'
import {materials} from './modules/materials'
import {departments} from './modules/departments'
import {storehouses} from './modules/storehouses'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    suppliers,
    materials,
    departments,
    storehouses
  }
})
